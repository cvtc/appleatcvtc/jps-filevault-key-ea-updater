import logging
from concurrent.futures import ThreadPoolExecutor
from os import environ

from jps_api_wrapper.classic import Classic
from jps_api_wrapper.pro import Pro

logging.basicConfig(
    level=logging.INFO, format="%(asctime)s - %(levelname)s - %(message)s"
)


def main():
    JPS_URL = environ["JPS_URL"]
    JPS_USERNAME = environ["JPS_USERNAME"]
    JPS_PASSWORD = environ["JPS_PASSWORD"]
    ADVANCED_SEARCH_ID = environ["ADVANCED_SEARCH_ID"]
    FILEVAULT_EA_ID = environ["FILEVAULT_EA_ID"]
    FILEVAULT_POLICY_NAME = environ["FILEVAULT_POLICY_NAME"]

    MULTITHREAD_WORKERS = int(environ["MULTITHREAD_WORKERS"])

    with Classic(JPS_URL, JPS_USERNAME, JPS_PASSWORD) as classic, Pro(
        JPS_URL, JPS_USERNAME, JPS_PASSWORD
    ) as pro:
        executor = ThreadPoolExecutor(max_workers=MULTITHREAD_WORKERS)
        computers = classic.get_advanced_computer_search(ADVANCED_SEARCH_ID)[
            "advanced_computer_search"
        ]["computers"]
        computer_ids = [computer["id"] for computer in computers]
        computer_existing_eas = {
            computer["id"]: computer["FileVault___Key_Viewed"] for computer in computers
        }

        computer_histories = [
            result
            for result in executor.map(classic.get_computer_history, computer_ids)
        ]

        for computer_history in computer_histories:
            computer_id = computer_history["computer_history"]["general"]["id"]
            key_view_time = 0
            key_creation_time = 0

            policy_logs = computer_history["computer_history"]["policy_logs"]
            audit_logs = computer_history["computer_history"]["audits"]

            for policy_log in policy_logs:
                if policy_log["policy_name"] == FILEVAULT_POLICY_NAME:
                    key_creation_time = policy_log["date_completed_epoch"]
                    break

            for audit_log in audit_logs:
                if audit_log["event"] == "Viewed FileVault Encryption Key":
                    key_view_time = audit_log["date_time_epoch"]
                    break

            if key_view_time:
                if (
                    key_view_time > key_creation_time
                    and computer_existing_eas[computer_id] != "True"
                ):
                    pro.update_computer_inventory(
                        {
                            "extensionAttributes": [
                                {
                                    "definitionId": FILEVAULT_EA_ID,
                                    "values": ["True"],
                                }
                            ]
                        },
                        computer_id,
                    )
                    logging.info(
                        f"Computer {computer_id} FileVault - Key Viewed EA updated to "
                        "True"
                    )
                if (
                    key_view_time < key_creation_time
                    and computer_existing_eas[computer_id] != "False"
                ):
                    pro.update_computer_inventory(
                        {
                            "extensionAttributes": [
                                {
                                    "definitionId": FILEVAULT_EA_ID,
                                    "values": ["False"],
                                }
                            ]
                        },
                        computer_id,
                    )
                    logging.info(
                        f"Computer {computer_id} FileVault - Key Viewed EA updated to "
                        "False"
                    )


if __name__ == "__main__":
    main()
