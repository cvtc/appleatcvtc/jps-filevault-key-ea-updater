import azure.functions as func

import jps_filevault_key_ea_updater

app = func.FunctionApp()

"""
These need to be split across two days since Azure only uses UTC time.
"""


@app.schedule(
    schedule="0 0 11-23 * * *",
    arg_name="myTimer",
    run_on_startup=False,
    use_monitor=False,
)
def Hourly(myTimer: func.TimerRequest) -> None:
    jps_filevault_key_ea_updater.main()
