JPS FileVault Key EA Updater
=======================================
Updates the FileVault - Key Viewed Extension Attribute based on policy logs of the last key rotation and audit logs of the last time the key was viewed.


## Reasoning for script
There isn't currently a way to get this information in the JPS and we wanted a way to easily scope computers that have had their FileVault recovery keys used scoped to cycle their keys.


## Requirements
An Extension Attribute with the following settings:
* Name
    * FileVault - Key Viewed
* Data Type
    * String
* Input Type
    * Pop-up Menu
        * False
        * True

Computer Policy that issues new FileVault recovery keys (you can set this up with the following settings)
* Disk Encryption
    * Action
        * Issue New Recovery Key
            * Recovery Key Type
                * Individual

Advanced computer search with the following settings:
* Criteria
    * Managed is Managed and
    * FileVault 2 Individual Key Validation is Valid
* Display
    * Extension Attributes
        * FileVault - Key Viewed (Or whatever you set your FileVault key EA name to)

## Usage

### Local
Export the variables listed below into your environment:
* All variable values are described below.
* The first command will make your bash history ignore commands with whitespace infront of them. This will make it so your JPS_PASSWORD isn't in 
your terminal history.
```console
setopt HIST_IGNORE_SPACE
export JPS_URL=https://example.jamfcloud.com
export JPS_USERNAME=exampleUsername
 export JPS_PASSWORD=examplePassword
export ADVANCED_SEARCH_ID=1001
export FILEVAULT_EA_ID=42
export FILEVAULT_POLICY_NAME="FileVault - Key Viewed"
export MULTITHREAD_WORKERS=5
```

Run these commands to install all dependencies and run the script:
```console
pip install pipenv
pipenv install --deploy
pipenv run python ./jps_filevault_key_ea_updater.py
```

### GitLab
Uses base Azure Function setup, consult [general documentation.](https://gitlab.com/cvtc/appleatcvtc/project-templates/-/tree/main/Python%20Azure%20Functions?ref_type=heads)


## Variables

### JPS_URL
* URL of your JPS Server
> https://example.jamfcloud.com

### JPS_USERNAME / JPS_PASSWORD
JPS account credentials with the following permissions set:
* Jamf Pro Server Objects
    * Advanced Computer Searches
        * Read
    * Computers
        * Read
        * Update

### ADVANCED_SEARCH_ID
ID of an advanced computer search with the following settings:
* Criteria
    * Managed is Managed and
    * FileVault 2 Individual Key Validation is Valid
* Display
    * Extension Attributes
        * FileVault - Key Viewed (Or whatever you set your FileVault key EA name to)

### FILEVAULT_EA_ID
ID of the FileVault - Key Viewed EA

### FILEVAULT_POLICY_NAME
Name of the policy that issues new recovery keys
* We are using File Vault: Issue New Recovery Key

### MULTITHREAD_WORKERS
Number of multithread workers to run Jamf API requests, a higher number will complete the script faster. I would recommend setting it to no higher than 5 because otherwise the JPS will start returning 502 errors on some requests.

## Authors

Bryan Weber (bweber26@cvtc.edu)

## Copyright

JPS FileVault Key Rotater is Copyright 2023 Chippewa Valley Technical College. It is provided under the MIT License. For more information, please see the LICENSE file in this repository.
